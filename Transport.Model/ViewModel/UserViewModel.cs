﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Transport.Data.Entities;
using Transport.Model.Infraestructure;

namespace Transport.Model.ViewModel
{
    public class UserViewModel : BaseViewModel
    {



        [MaxLength(50)]
        public String FirstName { get; set; }

        [MaxLength(50)]
        public String LastName { get; set; }

        public DateTime BirthDay { get; set; }

        public String Email { get; set; }
        
        public String UserName { get; set; }

        public User User { get; set; }

        public bool IsActive { get; set; }

        public List<Viaje> Viaje  { get; set; }
        

    }
}