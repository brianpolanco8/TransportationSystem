﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Transport.Data.Entities;
using Transport.Model.Infraestructure;

namespace Transport.Model.ViewModel
{
    public class ViajeViewModel : BaseViewModel
    {
        public string Route { get; set; }
        public string Destination { get; set; }
        public string Origin { get; set; }
        public int Price { get; set; }
        public DateTime DepartureTime { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public Viaje Viaje { get; set; }
        public List<User> User { get; set; }



    }
}
