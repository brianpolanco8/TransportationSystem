﻿using System;
using System.Collections.Generic;
using System.Text;
using Transport.Model.Infraestructure;

namespace Transport.Model.ViewModel
{
    public class UserProfileViewModel : BaseViewModel
    {
        public Int32 UserId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        //public byte?[] Picture { get; set; }
    }
}
