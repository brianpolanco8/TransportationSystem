﻿using System;
using System.Collections.Generic;
using System.Text;
using Transport.Data.Entities;
using Transport.Model.Infraestructure;

namespace Transport.Model.ViewModel
{
    public class FacturaViewModel : BaseViewModel
    {
        public int IdFactura { get; set; }
        public int Payment { get; set; }
        public int Change { get; set; }
        public String PaymentMethod { get; set; }
        public ViajeViewModel viaje { get; set; }
        public UserViewModel user { get; set; }
        public int userId { get; set; }
        //public List<Viaje> Viaje { get; set; }

    }
}
