﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transport.Data.Entities;
using Transport.Model.ViewModel;
using Transport.Service.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Transport.UI.Controllers
{
    public class UsersController : Controller
    {

        private readonly IUserService userService;
        private readonly IViajeService viajeService;
        //private readonly IUserProfileService userProfileService;

        public UsersController(IUserService _userService, IViajeService _viajeService
                               //, IUserProfileService _userProfileService
                              )
        {
            this.userService = _userService;
            this.viajeService = _viajeService;
            //this.userProfileService = _userProfileService;
        }



        [HttpGet]
        public ActionResult Index()
        {
            var viajes = (List<ViajeViewModel>)viajeService.GetAll().ResultObject;
            var users = (List<UserViewModel>)userService.GetAll().ResultObject;
            var filter = users.Where(x => x.IsActive == true).ToList();
            if (users == null)
                return View(new List<ViajeViewModel>());


            //var viajes = viajeService.GetAll().ResultObject;
            //if (viajes == null)
                //return View(new List<ViajeViewModel>());

            //var userProfile = userProfileService.GetAll();
            return View(filter);
        }

        [HttpGet("users/details/{id}")]
        public ActionResult Details(int id)
        {
            var users = (List<UserViewModel>)userService.GetAll().ResultObject;
            var viajes = (List<ViajeViewModel>)viajeService.GetAll().ResultObject;
            var filter = viajes.Where(x => x.UserId == id).ToList();


            return View(filter);
        }

        [HttpGet("users/add")]
        public ActionResult UserInsert()
        {
            var users = (List<UserViewModel>)userService.GetAll().ResultObject;
            var count = users.Count + 1;
            UserViewModel vm = new UserViewModel
            {
                IsActive = true,
                Id = count
            };
            return View("UserForm", vm);
        }

        [HttpPost("users/add")]
        public ActionResult UserForm(UserViewModel userViewModel)
        {
            var result = userService.Insert(userViewModel).ResultObject;

            return RedirectToAction("Index", "Users");
        }

        [HttpGet("users/edit/{id}")]
        public ActionResult Edit(int id)
        {
            var users = (List<UserViewModel>)userService.GetAll().ResultObject;
            var filter = users.Where(x => x.Id == id).ToList();
            UserViewModel vm = new UserViewModel
            {
                IsActive = true,
            };

            return View("UserEdit", vm);
        }

        [HttpPost("users/edit/{id}")]
        public ActionResult UserEdit(UserViewModel userViewModel)
        {
            var users = userService.Update(userViewModel).ResultObject;
            UserViewModel vm = new UserViewModel
            {
                IsActive = true,
            };

            return RedirectToAction("Index", "Users");


        }

       [HttpGet("users/delete/{id}")]
        public ActionResult Delete(int id)
        {
            var users = (List<UserViewModel>)userService.GetAll().ResultObject;
            var filter =users.Where(x => x.Id == id).ToList();
            UserViewModel vm = filter[0];

            vm.IsActive = true;
            return View("UserDelete", vm);
        }

        [HttpPost("users/delete/{id}")]
        public ActionResult deleteUser(UserViewModel userViewModel)
        {
            userViewModel.IsActive = false;
            var users = userService.Update(userViewModel).ResultObject;
            
            return RedirectToAction("Index", "Users");
        }



    }
}
