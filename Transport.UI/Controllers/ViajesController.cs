﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transport.Model.ViewModel;
using Transport.Service.Implementations;
using Transport.Service.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Transport.UI.Controllers
{
    public class ViajesController : Controller
    {
        private readonly IViajeService viajeService;
        private readonly IFacturaService facturaService;
        private readonly IUserService userService;
        //private readonly IUserProfileService userProfileService;

        public ViajesController(IViajeService _viajeService, IFacturaService _facturaService, IUserService _userservice
                               //, IUserProfileService _userProfileService
                               )
        {
            this.viajeService = _viajeService;
            this.facturaService = _facturaService;
            this.userService = _userservice;
            //this.userProfileService = _userProfileService;
        }

        public ActionResult Index()
        {

            var users = userService.GetAll().ResultObject;
            var viajes = viajeService.GetAll().ResultObject;
            if (viajes == null)
                return View(new List<ViajeViewModel>());

            //var userProfile = userProfileService.GetAll();
            return View(viajes);
        }


        [HttpGet("viajes/details/{id}")]
        public ActionResult Details(int id)
        {
            //var result = (LviajeService.GetByUserId(userId).ResultObject;

            var viajes = (List<FacturaViewModel>)facturaService.GetAll().ResultObject;
            var filter = viajes.Where(x => x.Id == id).ToList();



            return View(filter);
        }

        [HttpGet("viajes/add")]
        public ActionResult ViajeInsert()
        {
            var viajes = (List<ViajeViewModel>)viajeService.GetAll().ResultObject;
            var count = viajes.Count + 1;
            ViajeViewModel vm = new ViajeViewModel
            {
                Id = count
            };
            return View("ViajeForm", vm);
        }

        [HttpPost("viajes/add")]
        public ActionResult ViajeForm(ViajeViewModel viajeViewModel)
        {
            var viajes = (List<ViajeViewModel>)viajeService.GetAll().ResultObject;
            var count = viajes.Count + 1;
            viajeViewModel.Id = count;
            viajeViewModel.UserId = 1;

            var result = viajeService.Insert(viajeViewModel).ResultObject;

            return RedirectToAction("Index", "Viajes");
        }

        [HttpGet("viajes/edit/{id}")]
        public ActionResult Edit(int id)
        {
            var viajes = (List<ViajeViewModel>)viajeService.GetAll().ResultObject;
            var filter = viajes.Where(x => x.Id == id).ToList();
            ViajeViewModel vm = new ViajeViewModel();
            vm = filter[0];
            return View("ViajeEdit", vm);
        }

        [HttpPost("viajes/edit/{id}")]
        public ActionResult ViajeEdit(ViajeViewModel vm)
        {
            vm.UserId = 1;
            var result = viajeService.Update(vm).ResultObject;

            return RedirectToAction("Index", "Viajes");
        }

        [HttpGet("viajes/delete/{id}")]
        public ActionResult Delete(int id)
        {
            var users = (List<ViajeViewModel>)viajeService.GetAll().ResultObject;
            var filter = users.Where(x => x.Id == id).ToList();

            return RedirectToAction("Details", "Users");
        }



    }


}

