﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transport.Data.Entities;
using Transport.Model.ViewModel;
using Transport.Service.Implementations;
using Transport.Service.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Transport.UI.Controllers
{
    public class FacturasController : Controller
    {

        private readonly IFacturaService facturaService;
        private readonly IUserService userService;
        private readonly IViajeService viajeService;
     //   private readonly IUserProfileService userProfileService;

        public FacturasController(IUserService _userService, IFacturaService _facturaService, IViajeService _viajeService
                             // , IUserProfileService _userProfileService
                              )
        {
            this.facturaService = _facturaService;
            this.viajeService = _viajeService;
            this.userService = _userService;
            //     this.userProfileService = _userProfileService;
        }

        public List<ViajeViewModel> viajesSeleccionados;


        [HttpGet]
        public ActionResult Index()
        {
            var facturas = (List<FacturaViewModel>)facturaService.GetAll().ResultObject;
            if (facturas == null)
                return View(new List<FacturaViewModel>());


            //var viajes = viajeService.GetAll().ResultObject;
            //if (viajes == null)
            //return View(new List<ViajeViewModel>());

            //var userProfile = userProfileService.GetAll();
            return View(facturas);
        }

        [HttpGet("facturas/addUser")]
        public ActionResult FacturaInsert()
        {
            var viajes = (List<ViajeViewModel>)viajeService.GetAll().ResultObject;
            var users = (List<UserViewModel>)userService.GetAll().ResultObject;
            var filter = users.Where(x => x.IsActive == true).ToList();

            if (users == null)
                return View(new List<UserViewModel>());


            return View("userFactura", filter);
        }

        [HttpGet("facturas/addTravel/{id}")]
        public ActionResult FacturaInsert(int userId)
        {
            var viajes = (List<ViajeViewModel>)viajeService.GetAll().ResultObject;
        //    var users = (List<UserViewModel>)userService.GetAll().ResultObject;

            if (viajes == null)
                return View(new List<ViajeViewModel>());


            return View("viajeFactura", viajes);
        }


        [HttpGet("facturas/Pago/{id}")]
        public ActionResult Pago(int viajeId)
        {
            FacturaViewModel fvm = new FacturaViewModel();
            fvm.userId = 1;
            var facturas = (List<FacturaViewModel>)facturaService.GetAll().ResultObject;
            var viajes = (List<ViajeViewModel>)viajeService.GetAll().ResultObject;
            var users = (List<UserViewModel>)userService.GetAll().ResultObject;
            var filtro = users.Where(x => x.Id == 1).ToList();
            var filter = viajes.Where(x => x.Id == 1).ToList();
            fvm.Id = facturas.Count() + 1;
            fvm.viaje = filter[0];
            fvm.user = filtro[0];
         
            return View("pagoFactura", fvm);
            
        }

        [HttpPost("facturas/Pago/{id}")]
        public ActionResult pagoFactura(FacturaViewModel facturaViewModel)
        {
            
            var facturas = (List<FacturaViewModel>)facturaService.GetAll().ResultObject;
            var viajes = (List<ViajeViewModel>)viajeService.GetAll().ResultObject;
            var users = (List<UserViewModel>)userService.GetAll().ResultObject;
            var filtro = users.Where(x => x.Id == 1).ToList();
            var filter = viajes.Where(x => x.Id == 1).ToList();
            facturaViewModel.Id = facturas.Count() + 1;
            facturaViewModel.userId = facturas.Count() + 1;
            facturaViewModel.viaje = filter[0];
            facturaViewModel.user = filtro[0];
            //  List<FacturaViewModel> listaFacturas = new List<FacturaViewModel>();
            //  listaFacturas.Add(fvm);
            var result = facturaService.Insert(facturaViewModel).ResultObject;

            return RedirectToAction("Index", "Facturas");

        }

    }
}
