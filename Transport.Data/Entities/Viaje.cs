﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Transport.Data.Entities
{
    public class Viaje : BaseEntity
    {
        public string Route { get; set; }
        public string Destination { get; set; }
        public string Origin { get; set; }
        public int Price { get; set; }
        public int UserId { get; set; }
        public List<User> User;
       


    }
}