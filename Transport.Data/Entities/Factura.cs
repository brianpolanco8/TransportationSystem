﻿using System;
using System.Collections.Generic;

namespace Transport.Data.Entities
{
    public class Factura : BaseEntity
    {
        public int Payment { get; set; }
        public int Change { get; set; }
        public String PaymentMethod { get; set; }
        public int ViajeId { get; set; }
        public List<Viaje> Viaje { get; set; }
        public int userId { get; set; }

    }
}