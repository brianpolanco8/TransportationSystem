﻿using System;
using System.Collections.Generic;
using System.Text;
using Transport.Data.Entities;

namespace Transport.Data.Entities
{
    public class UserProfile : BaseEntity
    {
        public Int32 UserId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        //public byte?[] Picture { get; set; }
    }
}
