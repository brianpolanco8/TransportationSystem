﻿using System;
using System.Collections.Generic;
using System.Text;
using Transport.Data.Entities;

namespace Transport.Data.Entities
{
    public class User : BaseEntity
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public DateTime BirthDay { get; set; }
        public String Email { get; set; }
        public String UserName { get; set; }
        public bool IsActive { get; set; }
        public List<Viaje> Viaje { get; set; }




    }
}
