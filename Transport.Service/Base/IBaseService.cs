﻿using System;
using Transport.Model.Insfraestructure;

namespace Transport.Service.Base
{
    public interface IBaseService<T>
    {
        ServiceResult Insert(T viewModel);
        ServiceResult Update(T viewModel);
        ServiceResult Delete(T viewModel);
        ServiceResult GetAll();
        ServiceResult GetById(int id);

        //  ServiceResult GetByRowId(string rowId); NO LO USAMOS, LO COMENTAMOS
    }
}
