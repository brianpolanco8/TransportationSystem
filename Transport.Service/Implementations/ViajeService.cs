﻿using System;
using System.Collections.Generic;
using System.Linq;
using Transport.Data.Entities;
using Transport.Helpers.Infraestructure;
using Transport.Helpers.Utils;
using Transport.Model.Insfraestructure;
using Transport.Model.ViewModel;
using Transport.Repository.Framework;
using Transport.Service.Base;
using Transport.Service.Interfaces;

namespace Transport.Service.Implementations
{
    public class ViajeService :
    BaseService<ViajeViewModel, Viaje>, IViajeService
    {
        public ViajeService(
            IRepository<Viaje> viajeRepository)
            : base(viajeRepository)
        {

        }

        public ServiceResult GetByUserId(int userId)
        {
            ServiceResult serviceResult = new ServiceResult();

            var result = ((List<Viaje>)this.Repository.
                          GetAll(x => x.UserId == userId).Data).ToList();
            serviceResult.Success = true;
            serviceResult.ResultTitle = Error.GetErrorMessage(Error.CorrectTransaction);
            //serviceResult.Messages.Add(Error.GetErrorMessage(Error.CorrectTransaction));
            serviceResult.ResultObject = MapperHelper.Instance.Map<List<Viaje>, List<ViajeViewModel>>(result);

            return serviceResult;
        }
    }
}
