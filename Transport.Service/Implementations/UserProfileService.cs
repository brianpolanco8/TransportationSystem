﻿using System;
using Transport.Data.Entities;
using Transport.Model.ViewModel;
using Transport.Repository.Framework;
using Transport.Service.Base;
using Transport.Service.Interfaces;

namespace Transport.Service.Implementations
{
    public class UserProfileService :
    BaseService<UserProfileViewModel, UserProfile>, IUserProfileService
    {
        public UserProfileService(
            IRepository<UserProfile> userProfileRepository)
            : base(userProfileRepository)
        {

        }
    }
}
