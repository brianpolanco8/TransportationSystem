﻿using System;
using Transport.Data.Entities;
using Transport.Helpers.Infraestructure;
using Transport.Helpers.Utils;
using Transport.Model.ViewModel;
using Transport.Model.Insfraestructure;
using Transport.Repository.Framework;
using Transport.Service.Base;
using Transport.Service.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Transport.Service.Implementations
{
    public class UserService :
    BaseService<UserViewModel, User>, IUserService
    {

        public UserService(
            IRepository<User> userRepository)
            : base(userRepository)
        {

        }

        //public ServiceResult ValidateUser(string username, string password)
        //{
        //    ServiceResult serviceResult = new ServiceResult();

        //    serviceResult.Success = true;
        //    serviceResult.ResultTitle = Error.GetErrorMessage(Error.CorrectTransaction);
        //    serviceResult.Messages.Add(Error.GetErrorMessage(Error.CorrectTransaction));
        //    serviceResult.ResultObject = MapperHelper.Instance.
        //        Map<User, UserViewModel>(
        //            this.Repository.GetAll(i => i.UserName == username
        //                                  // && i.Password == password
        //                                  ).Data);

        //    return serviceResult;

        //}

        //public ServiceResult Find(string username)
        //{
        //    ServiceResult serviceResult = new ServiceResult();

        //    serviceResult.Success = true;
        //    serviceResult.ResultTitle = Error.GetErrorMessage(Error.CorrectTransaction);
        //    serviceResult.Messages.Add(Error.GetErrorMessage(Error.CorrectTransaction));
        //    serviceResult.ResultObject = MapperHelper.Instance.
        //        Map<User, UserViewModel>(
        //            this.Repository.GetAll(i => i.UserName == username
        //                                   ).Data);

        //    return serviceResult;
        //}

       
    }
}
