﻿using System;
using Transport.Data.Entities;
using Transport.Model.ViewModel;
using Transport.Repository.Framework;
using Transport.Service.Base;
using Transport.Service.Interfaces;

namespace Transport.Service.Implementations
{
    public class FacturaService :
    BaseService<FacturaViewModel, Factura>, IFacturaService
    {
        public FacturaService(
            IRepository<Factura> FacturaRepository)
            : base(FacturaRepository)
        {

        }
    }
}