﻿using System;
using Transport.Model.ViewModel;
using Transport.Service.Base;

namespace Transport.Service.Interfaces
{
    public interface IUserProfileService : IBaseService<UserProfileViewModel>
    {

    }
}
