﻿using System;
using Transport.Model.Insfraestructure;
using Transport.Model.ViewModel;
using Transport.Service.Base;

namespace Transport.Service.Interfaces
{
    public interface IViajeService : IBaseService<ViajeViewModel>
    {
        ServiceResult GetByUserId(int userId);
    }
}
