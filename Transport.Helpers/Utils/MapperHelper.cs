﻿using System;
using AutoMapper;
using Transport.Data.Entities;
using Transport.Model.ViewModel;

namespace Transport.Helpers.Utils
{
    public class MapperHelper
    {
        static MapperHelper _instance;

        MapperHelper()
        {
            Mapper.Initialize(conf =>
            {
                conf.CreateMap<User, UserViewModel>();
                conf.CreateMap<UserProfile, UserProfileViewModel>();
                conf.CreateMap<Factura, FacturaViewModel>();
                conf.CreateMap<Viaje, ViajeViewModel>();
            });
        }

        public static MapperHelper Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MapperHelper();
                }

                return _instance;
            }
        }

        public To Map<From, To>(From obj)
        {
            return Mapper.Map<To>(obj);
        }

        public To Map<From, To>(From from, To to)
        {
            return Mapper.Map(from, to);
        }
    }
}
