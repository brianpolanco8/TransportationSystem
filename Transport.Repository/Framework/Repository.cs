﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Transport.Data.Infraestructure;

namespace Transport.Repository.Framework
{
    public class Repository <T> : IRepository<T> where T : class
    {

        //DEFINIENDO EL APPLICATION CONTEXT QUE SERA LA BASE DE DATOS, SE UTILIZA
        //GANAR ACCESO A LA BASE DE DATOS
        private readonly ApplicationContext context;



        //DEFINIENDO EL DBSET QUE SERA LA TABLA, SE UTILIZA PARA HACER OPERACION
        //CON LAS TABLAS
        private DbSet<T> dbSet;

        //STRING PARA CONNECTARSE A LA BASE DE DATOS
        string connectionString = String.Empty;

        public Repository(ApplicationContext context)
        {
            if(context == null )
            {
                throw new ArgumentNullException("Conexion no establecida");
            }

            this.context = context;
            dbSet = context.Set<T>();
            connectionString = context.Database.GetDbConnection().ConnectionString;
        }

        //Para eliminar registros
        public DataResult Delete(T entity)
        {
            DataResult result = new DataResult();

            try
            {
                dbSet.Remove(entity);
                context.SaveChanges();
                result.Successfull = true;
            }

            catch (Exception ex)
            {
                result.LogError(ex);
                result.Successfull = false;
            }

            return result;
        }


        //SELECCIONAR *  REGISTROS DEL DBSET
        public DataResult GetAll()
        {
            DataResult result = new DataResult();

            try
            {
                result.Data = dbSet.ToList();
                result.Successfull = true;
            }

            catch(Exception ex)
            {
                result.LogError(ex);
                result.Successfull = false;
            }

            return result;
        }

        //SELECT * FROM DBSET WHERE CAMPO = N
        //public DataResult GetAll(Expression<Func<T, bool>> specificaiton, params Expression<Func<T, object>>[] includeProperties)
        //{
        //    DataResult result = new DataResult();

        //    try
        //    {
        //        if(includeProperties.Length > 0)
        //        {
        //            result.Data = GetAllIncluding(includeProperties)
        //                .Where(specificaiton).ToList();

        //            result.Successfull = true;
        //        }
        //    }

        //    catch(Exception ex)
        //    {
        //        result.LogError(ex);
        //        result.Successfull = false;
        //    }

        //    return result;
        //}

        //SELECT * FROM DBSET WHERE ID = id
        public DataResult GetById(int id)
        {
            DataResult result = new DataResult();

            try
            {
                //FIND VINE POR DEFECTO CON DBSET
                result.Data = dbSet.Find(id);
                result.Successfull = true;
            }

            catch(Exception ex)
            {
                result.LogError(ex);
                result.Successfull = false;
            }

            return result;
        }

        public DataResult GetViajeByUserId(int userId)
        {
            throw new NotImplementedException();
        }

        //INSERT INTO
        public DataResult Insert(T entity)
        {
            DataResult result = new DataResult();

            try
            {
                dbSet.Add(entity);
                context.SaveChanges();
                result.Successfull = true;
            }

            catch(Exception ex)
            {
                result.LogError(ex);
                result.Successfull = false;
            }

            return result;
        }

        public bool SaveChanges()
        {
            //SAVECHANGES es una funcion por defecto del context que guarda
            //los cambios hechos a este


            return context.SaveChanges() > 0;
        }

        public DataResult Update(T entity)
        {
            DataResult result = new DataResult();

            try
            {
                dbSet.Update(entity);
                context.SaveChanges();
                result.Data = entity;
                result.Successfull = true;
            }

            catch(Exception ex)
            {
                result.LogError(ex);
                result.Successfull = false;
            }

            return result;
        }

        DataResult IRepository<T>.Delete(T entity)
        {
            DataResult result = new DataResult();

            try
            {
                dbSet.Update(entity);
                context.SaveChanges();
                result.Data = entity;
                result.Successfull = true;

            }

            catch (Exception ex)
            {
                result.LogError(ex);
                result.Successfull = false;
            }


            return result;
        }

        DataResult IRepository<T>.GetAll()
        {
            DataResult dataResult = new DataResult();

            dataResult.Data = dbSet.ToList();
            dataResult.Successfull = true;

            return dataResult;
        }

        DataResult IRepository<T>.GetAll(Expression<Func<T, bool>> specificaiton, params Expression<Func<T, object>>[] includeProperties)
        {
            DataResult dataresult = new DataResult();

            dataresult.Data = dbSet.ToList();
            dataresult.Successfull = true;

            return dataresult;
        }

        private IQueryable<T> GetAllIncluding(
            params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> queryable = dbSet;
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                queryable = queryable.Include<T, object>(includeProperty);
            }

            return queryable;
        }

        DataResult IRepository<T>.GetById(int id)
        {
            DataResult result = new DataResult();

            try
            {
                //FIND VINE POR DEFECTO CON DBSET
                result.Data = dbSet.Find(id);
                result.Successfull = true;
            }

            catch (Exception ex)
            {
                result.LogError(ex);
                result.Successfull = false;
            }

            return result;
        }

        DataResult IRepository<T>.Insert(T entity)
        {
            DataResult result = new DataResult();

            try
            {
                dbSet.Add(entity);
                context.SaveChanges();
                result.Successfull = true;
            }

            catch (Exception ex)
            {
                result.LogError(ex);
                result.Successfull = false;
            }

            return result;
        }

        DataResult IRepository<T>.Update(T entity)
        {
            DataResult result = new DataResult();

            try
            {
                dbSet.Update(entity);
                context.SaveChanges();
             //   result.Data = entity;
                result.Successfull = true;

            }

            catch (Exception ex)
            {
                result.LogError(ex);
                result.Successfull = false;
            }


            return result;
        }
    }
}
