﻿using System;
using System.Linq.Expressions;
using Transport.Data.Infraestructure;

namespace Transport.Repository.Framework
{
    public interface IRepository<T>
    {

        //SE DEFINEN LOS METHODS TIPO DATARESULT QUE SE VAN A USAR EN EL RESPOSITORIO
        DataResult GetAll();

        DataResult GetAll(Expression<Func<T, bool>> specificaiton,
                          params Expression<Func<T, object>>[] includeProperties);



        DataResult GetById(int id);


        DataResult Insert(T entity);

        DataResult Update(T entity);

        DataResult Delete(T entity);

        Boolean SaveChanges();
    }
}
